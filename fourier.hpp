#pragma once

#include "fixed_size_fifo.hpp"
#include "low_pass_filter.hpp"

#include <vector>
#include <complex>
#include <numbers>


class Fourier {
public:
  Fourier(unsigned int size, unsigned int resolution, double frequency_cutoff_high, double frequency_cutoff_low = 2);

  void push(double value);

  double get(double x);

  inline double getByIndex(int index) {
    // Call the parent function.
    return get(indexToOffset(index));
  }

  double getSimplified(double x, unsigned int frequencies = 1);

  inline double getSimplifiedByIndex(int index, unsigned int frequencies = 1) {
    // Call the parent function.
    return getSimplified(indexToOffset(index), frequencies);
  }

  inline double indexToOffset(int index) const {
    // Convert the index to and offset.
    return static_cast<double>(index) / static_cast<double>(size);
  }

  bool isReady() const;

protected:
  double calculateFrequencyComponent(const std::vector<std::complex<double>>::const_iterator &iterator, const double x) const;

  int getIndex(const std::vector<std::complex<double>>::const_iterator &iterator) const;
  double getFrequency(const std::vector<std::complex<double>>::const_iterator &iterator) const;
  double getFrequencyFromIndex(int index) const;
  double getAmplitude(const std::vector<std::complex<double>>::const_iterator &iteratordouble) const;
  double getAmplitudeSquared(const std::vector<std::complex<double>>::const_iterator &iterator) const;
  double getPhase(const std::vector<std::complex<double>>::const_iterator &iterator) const;
  std::complex<double> getCorrected(const std::vector<std::complex<double>>::const_iterator &iterator) const;
  double getAngularVelocity(const std::vector<std::complex<double>>::const_iterator &iterator) const;
  double getAngularVelocityFromFrequency(double frequency) const;
  double getAngularVelocityFromIndex(int index) const;
  double getAngularIncrementFromVelocity(double velocity) const;
  double getAngularIncrementFromFrequency(double frequency) const;
  double getAngularIncrementFromIndex(int index) const;

  LowPassFilter<double> input_estimate;
  FixedSizeFifo<double> time_fifo;
  std::vector<std::complex<double>> frequency_vector;
  std::vector<std::complex<double>> precomputed_frequency_vector_static;
  std::vector<std::complex<double>> precomputed_frequency_vector_linear;
  LowPassFilter<double> static_estimate;
  LowPassFilter<double> slope_estimate;

  unsigned int push_operations;

  const unsigned int size;
  const unsigned int resolution;
  const double frequency_cutoff_high;
  const double frequency_cutoff_low;
};
