#pragma once

#include <vector>

template<typename T>
class FixedSizeFifo {
public:
  FixedSizeFifo(int size) : data(size) {
    current_index = 0;
  }

  T push(T value) {
    current_index = (current_index + 1) % data.size();

    const T result = data[current_index];
    data[current_index] = value;

    return result;
  }

  T peak_front(unsigned int i = 0) {
    return data[(current_index - (i % data.size()) + data.size()) % data.size()];
  }

  T peak_back(unsigned int i = 0) {
    return data[(current_index + 1 + i) % data.size()];
  }

protected:
  std::vector<T> data;
  int current_index;
};