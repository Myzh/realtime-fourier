#pragma once

template<typename I>
class PassIterator : public I {
public:
  PassIterator(I &&content, int increment = 1) : I(content), increment(increment) {}
  PassIterator(I &content, int increment = 1) : I(content), increment(increment) {}

  void operator=(PassIterator<I> &value) {
    I::operator=(static_cast<I &>(value));
    increment = value.increment;
  }

  I &operator*() {
    return *this;
  }

  I const &operator*() const {
    return *this;
  }
  
  PassIterator &operator++() {
    static_cast<I &>(*this) += increment;
    return *this;
  }
  
  PassIterator operator++(int) const {
    return PassIterator(static_cast<I &>(*this) + increment, increment);
  }

  PassIterator &operator--() {
    static_cast<I &>(*this) -= increment;
    return *this;
  }
  
  PassIterator operator--(int) const {
    return PassIterator(static_cast<I &>(*this) - increment, increment);
  }

  PassIterator operator+=(int n) {
    static_cast<I &>(*this) += n * increment;
    return PassIterator(static_cast<I &>(*this), increment);
  }

  PassIterator operator-=(int n) {
    static_cast<I &>(*this) -= n * increment;
    return PassIterator(static_cast<I &>(*this), increment);
  }

  PassIterator operator+(int n) const {
    return PassIterator(static_cast<I &>(*this) + n * increment, increment);
  }

  PassIterator operator-(int n) const {
    return PassIterator(static_cast<I &>(*this) - n * increment, increment);
  }

  int operator+(const PassIterator& rhs) const {
    return (static_cast<I>(*this) + static_cast<I>(*rhs)) / increment;
  }

  int operator-(const PassIterator& rhs) const {
    return (static_cast<I>(*this) - static_cast<I>(*rhs)) / increment;
  }

  PassIterator operator[](int n) const {
    return PassIterator(static_cast<I>(*this) + (n * increment), increment);
  }

protected:
  int increment;
};
