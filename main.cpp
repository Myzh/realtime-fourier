#include "fourier.hpp"
#include <iostream>
#include <iomanip>
#include <numbers>
#include <fstream>
#include <string>
#include <sstream>
#include <chrono>

#include "low_pass_filter.hpp"

int main(int argc, char **argv) {
  if (argc < 9) {
    std::cout << "Usage: " << argv[0] << " <input file> <output file> <size> <resolution> <increment> <frequency_cutoff_high> <frequencies> <lookahead>" << std::endl;
    return 1;
  }

  const std::string input_filename = argv[1];
  std::cout << "Using the input filename " << input_filename << std::endl;
  std::ifstream input(input_filename);

  const std::string output_filename = argv[2];
  std::cout << "Using the output filename " << output_filename << std::endl;
  std::ofstream output(output_filename);

  const int size = std::atoi(argv[3]);
  std::cout << "Using the size " << size << std::endl;

  const int resolution = std::atoi(argv[4]);
  std::cout << "Using the resolution " << resolution << std::endl;

  const int increment = std::atoi(argv[5]);
  std::cout << "Using the increment " << increment << std::endl;

  const int frequency_cutoff_high = std::atoi(argv[6]);
  std::cout << "Using the frequency_cutoff_high " << frequency_cutoff_high << std::endl;

  const int frequencies = std::atoi(argv[7]);
  std::cout << "Using " << frequencies << " frequencies" << std::endl;

  const double lookahead = std::atof(argv[8]);
  std::cout << "Using lookahead of " << lookahead << std::endl;

  output << "size" << ";" << "resolution" << ";" << "frequency_cutoff_high" << ";" << "frequencies" << ";" << "lookahead" << ";" << std::endl;
  output << size << ";" << resolution << ";" << frequency_cutoff_high << ";" << frequencies << ";" << ";" << lookahead << ";" << std::endl;

  output << std::endl;

  std::chrono::time_point timestamp_parse_begin = std::chrono::high_resolution_clock::now();

  std::string line;
  std::getline(input, line);
  std::istringstream line_stream(line);

  std::vector<std::vector<double>> input_data;

  std::string cell;
  while (std::getline(line_stream, cell, ';')) {
    try {
      input_data.emplace_back();
      input_data.emplace_back();
    } catch (std::invalid_argument &) {
      std::cerr << "Invalid CSV data." << std::endl;
    }
  }

  int current_point = 0;
  while(std::getline(input, line)) {
    ++current_point;

    if (current_point % increment != 0) {
      continue;
    }

    std::istringstream line_stream(line);

    std::string cell;
    int i = 0;
    while (std::getline(line_stream, cell, ';')) {
      try {
        const double value = std::stod(cell);

        input_data[2 * i + 1].emplace_back(input_data[2 * i].empty() ? 0 : input_data[2 * i + 1][current_point / increment - 2] + value);
        input_data[2 * i].emplace_back(value);
      } catch (std::invalid_argument &) {
        std::cerr << "Invalid CSV data." << std::endl;
      }

      ++i;
    }
  }

  std::chrono::time_point timestamp_parse_end = std::chrono::high_resolution_clock::now();

  const long data_size_x = input_data.size();
  const long data_size_y = input_data[0].size();
  const long data_size = data_size_x * data_size_y;
  std::cout << "data size: " << data_size_x << "x" << data_size_y << " (" << data_size << ")" << std::endl;

  const std::chrono::milliseconds time_parse = std::chrono::duration_cast<std::chrono::milliseconds>(timestamp_parse_end - timestamp_parse_begin);
  std::cout << "parse: " << std::setw(10) << time_parse.count() << "ms (" << time_parse.count() / static_cast<double>(data_size) << "ms/item)" <<  std::endl;

  std::chrono::time_point timestamp_precomp_begin = std::chrono::high_resolution_clock::now();

  std::vector<Fourier> fourier_vector;
  fourier_vector.reserve(input_data.size());

  for (unsigned int i = 0; i < input_data.size(); ++i) {
    fourier_vector.emplace_back(size, resolution, frequency_cutoff_high);
  }

  std::chrono::time_point timestamp_precomp_end = std::chrono::high_resolution_clock::now();

  const std::chrono::milliseconds time_push = std::chrono::duration_cast<std::chrono::milliseconds>(timestamp_precomp_end - timestamp_precomp_begin);
  std::cout << "precomp: " << std::setw(10) << time_push.count() << "ms (" << time_push.count() / static_cast<double>(data_size_x * size) << "ms/item)" <<  std::endl;

  std::vector<std::vector<double>> realtime_data(input_data.size());
  std::vector<std::vector<double>> extrapolated_data(input_data.size());

  for (int i = 0; const std::vector<double> &row : input_data) {
    realtime_data[i].reserve(row.size() * 2);
    extrapolated_data[i].reserve(row.size() * 2);

    ++i;
  }

  std::chrono::time_point timestamp_push_begin = std::chrono::high_resolution_clock::now();

  for (int i = 0; const std::vector<double> &row : input_data) {
    Fourier &fourier = fourier_vector[i];

    for (const double &cell : row) {
      fourier.push(cell);

      realtime_data[i].push_back(fourier.getSimplified(0, frequencies));
      extrapolated_data[i].push_back(fourier.getSimplified(lookahead, frequencies));
    }

    ++i;
  }

  std::chrono::time_point timestamp_push_end = std::chrono::high_resolution_clock::now();

  const std::chrono::milliseconds time_precomp = std::chrono::duration_cast<std::chrono::milliseconds>(timestamp_push_end - timestamp_push_begin);
  std::cout << "push: " << std::setw(10) << time_precomp.count() << "ms (" << time_precomp.count() / static_cast<double>(data_size) << "ms/item)" <<  std::endl;


  std::vector<std::vector<double>> simplified_data(input_data.size());

  for (int i = 0; const std::vector<double> &row : input_data) {
    simplified_data[i].reserve(row.size() * 2);

    ++i;
  }

  std::chrono::time_point timestamp_get_begin = std::chrono::high_resolution_clock::now();

  for (int i = 0; i < data_size_x; ++i) {
    Fourier &fourier = fourier_vector[i];

    for (int j = 1 - data_size_y; j < data_size_y; ++j) {
      simplified_data[i].push_back(fourier.getSimplifiedByIndex(j, frequencies));
    }
  }

  std::chrono::time_point timestamp_get_end = std::chrono::high_resolution_clock::now();

  const std::chrono::milliseconds time_get = std::chrono::duration_cast<std::chrono::milliseconds>(timestamp_get_end - timestamp_get_begin);
  std::cout << "get: " << std::setw(10) << time_get.count() << "ms (" << time_get.count() / static_cast<double>(data_size) << "ms/item)" <<  std::endl;

  std::chrono::time_point timestamp_output_begin = std::chrono::high_resolution_clock::now();

  for (int x = 0; x < data_size_x; ++x) {
    output << "raw" << x << ";";
    output << "realtime" << x << ";";
    output << "filtered" << x << ";";
    output << "extrapolated" << x << ";";
  }

  output << std::endl;

  for (int y = 0; y < data_size_y; ++y) {
    for (int x = 0; x < data_size_x; ++x) {
      output << input_data[x][y] << ";";
      output << realtime_data[x][y] << ";";
      output << simplified_data[x][y] << ";";

      if (y > (size * lookahead)) {
        output << extrapolated_data[x][y - (size * lookahead)] << ";";
      } else {
        output << 0 << ";";
      }
    }

    output << std::endl;
  }

  for (int y = 0; y < size; ++y) {
    for (int x = 0; x < data_size_x; ++x) {
      Fourier &fourier = fourier_vector[x];

      output << 0 << ";";
      output << 0 << ";";
      output << simplified_data[x][y + data_size_y] << ";";
      output << 0 << ";";
    }

    output << std::endl;
  }

  std::chrono::time_point timestamp_output_end = std::chrono::high_resolution_clock::now();

  const std::chrono::milliseconds time_output = std::chrono::duration_cast<std::chrono::milliseconds>(timestamp_output_end - timestamp_output_begin);
  std::cout << "output: " << std::setw(10) << time_output.count() << "ms (" << time_output.count() / static_cast<double>(data_size) << "ms/item)" <<  std::endl;

  return 0;
}
