#include "fourier.hpp"
#include "pass_iterator.hpp"

#include <vector>
#include <complex>
#include <numbers>
#include <numeric>
#include <set>
#include <algorithm>


Fourier::Fourier(unsigned int size, unsigned int resolution, double frequency_cutoff_high, double frequency_cutoff_low) : input_estimate(frequency_cutoff_high, 1 / static_cast<double>(size)), time_fifo(size), frequency_vector(std::min<unsigned int>(frequency_cutoff_high, size / 2) * resolution), precomputed_frequency_vector_static(std::min<unsigned int>(frequency_cutoff_high, size / 2) * resolution), precomputed_frequency_vector_linear(std::min<unsigned int>(frequency_cutoff_high, size / 2) * resolution), static_estimate(frequency_cutoff_high, 1 / static_cast<double>(size)), slope_estimate(frequency_cutoff_low, 1 / static_cast<double>(size)), push_operations(0), size(size), resolution(resolution), frequency_cutoff_high(frequency_cutoff_high), frequency_cutoff_low(std::max(1.0, std::min(frequency_cutoff_low, frequency_cutoff_high))) {
  // The precomputation could also be done with the FFT algorithm.
  // This would improve the constructors complexity from O(n) = n^2 to O(n) = n*log(n).
  // However, as this is only an issue at startup, given proper use of this class, this is not required (so far).
  
  // Precompute the frequency spectrum of a constant signal (y[t] = 1).
  for (unsigned int i = 0; i < size; ++i) {
    // Iterate through the frequencies.
    for (int j = 0; std::complex<double> &frequency_value : precomputed_frequency_vector_static) {
      const double inverse_angualar_velocity = -getAngularVelocityFromIndex(j);

      // Rotate the current value with the angular velocity.
      const double theta = getAngularIncrementFromVelocity(inverse_angualar_velocity);
      frequency_value *= std::polar<double>(1, theta);

      // Add the function value to the frequency value.
      frequency_value += 1;

      ++j;
    }
  }
  
  // Precompute the frequency spectrum of a linear signal (y[t] = t + (size / 2)).
  for (unsigned int i = 0; i < size; ++i) {
    // Iterate through the frequencies.
    for (int j = 0; std::complex<double> &frequency_value : precomputed_frequency_vector_linear) {
      const double inverse_angualar_velocity = -getAngularVelocityFromIndex(j);

      // Rotate the current value with the angular velocity.
      const double theta = getAngularIncrementFromVelocity(inverse_angualar_velocity);
      frequency_value *= std::polar<double>(1, theta);

      // Add the function value to the frequency value.
      frequency_value += static_cast<double>(i) - (static_cast<double>(size) / 2);

      ++j;
    }
  }
}

void Fourier::push(double value) {
  const double filtered_value = input_estimate.push(value);
  const double old_value = time_fifo.push(filtered_value);

  // Iterate through the frequencies.
  for (int i = 0; std::complex<double> &frequency_value : frequency_vector) {
    const double inverse_angualar_velocity = -getAngularVelocityFromIndex(i);

    // Rotate the current value with the angular velocity.
    const double theta = getAngularIncrementFromVelocity(inverse_angualar_velocity);
    frequency_value *= std::polar<double>(1, theta);

    // Add the function value to the frequency value and subtract the old value with the correct phase.
    frequency_value -= old_value * std::polar<double>(1, inverse_angualar_velocity);
    frequency_value += filtered_value;

    ++i;
  }

  // Estimate the static and linear parts.
  static_estimate.push(frequency_vector[0].real() / size);
  slope_estimate.push((filtered_value - old_value) / size);

  if (push_operations < size) {
    ++push_operations;
  }
}

double Fourier::get(double x) {
  auto transform_lambda = [this, &x](std::vector<std::complex<double>>::const_iterator iterator) -> double {
    return calculateFrequencyComponent(iterator, x);
  };

  // Add all base frequency components together and correct the result.
  const double result = std::transform_reduce<PassIterator<std::vector<std::complex<double>>::const_iterator>>(PassIterator(frequency_vector.cbegin(), resolution), PassIterator(frequency_vector.cend(), resolution), 0.0, std::plus{}, transform_lambda);
  return result / size;
}

double Fourier::getSimplified(double x, unsigned int frequencies) {
  // Make sure the number of frequencies do not exceed the size of the frequency vector.
  const unsigned int number_frequencies = std::min<unsigned int>(frequencies, frequency_vector.size() - resolution);

  auto amplitide_compare_lambda = [this](const std::vector<std::complex<double>>::const_iterator &a, const std::vector<std::complex<double>>::const_iterator &b) -> bool {
    return getAmplitudeSquared(a) < getAmplitudeSquared(b);
  };

  // Set of the most significant frequencies.
  std::set<std::vector<std::complex<double>>::const_iterator, decltype(amplitide_compare_lambda)> filtered_elements(amplitide_compare_lambda);

  std::vector<std::complex<double>>::const_iterator current_value = frequency_vector.cbegin() + frequency_cutoff_low * resolution;
  std::vector<std::complex<double>>::const_iterator last_peak = current_value;

  // Fill the set.
  for (unsigned int i = frequency_cutoff_low * resolution; i <= frequency_cutoff_high * resolution; ++i) {
    ++current_value;

    // Find the largest element within a certain range and add it to the set.
    if (getAmplitudeSquared(current_value) > getAmplitudeSquared(last_peak)) {
      last_peak = current_value;
    } else if ((current_value - last_peak > resolution) || (i == frequency_cutoff_high * resolution)) {
      filtered_elements.emplace_hint(filtered_elements.begin(), last_peak);
      last_peak = current_value;

      // Only keep the wanted number of elements in the set.
      if (filtered_elements.size() > number_frequencies) {
        filtered_elements.erase(filtered_elements.begin());
      }
    }
  }
   
  const double static_part = static_estimate.get();
  const double linear_part = slope_estimate.get() * (x + 0.5) * size;

  auto position_lambda = [this, &x](const std::vector<std::complex<double>>::const_iterator &iterator) -> double {
    return calculateFrequencyComponent(iterator, x);
  };

  // Get the values of the selected frequencies at the corrected x and add them together.
  const double frequency_part = 2 * std::transform_reduce(filtered_elements.cbegin(), filtered_elements.cend(), 0.0, std::plus{}, position_lambda) / size;

  // Return all parts of the function.
  return static_part + linear_part + frequency_part;
}

bool Fourier::isReady() const {
  return push_operations >= size;
}

double Fourier::calculateFrequencyComponent(const std::vector<std::complex<double>>::const_iterator &iterator, const double x) const {
  // Remove the static and linear parts of this frequency.
  const std::complex<double> local_complex = getCorrected(iterator);

  // Rotate the value accordingly.
  const double theta = -x * getAngularVelocity(iterator);
  const std::complex result = local_complex * std::polar<double>(1, theta);

  // Return the real component.
  return result.real();
}

int Fourier::getIndex(const std::vector<std::complex<double>>::const_iterator &iterator) const {
  const int result = static_cast<int>(iterator - frequency_vector.cbegin());

  return result;
}

double Fourier::getFrequency(const std::vector<std::complex<double>>::const_iterator &iterator) const {
  return getFrequencyFromIndex(getIndex(iterator));
}

double Fourier::getFrequencyFromIndex(int index) const {
  return static_cast<double>(index) / static_cast<double>(resolution);
}

double Fourier::getAmplitude(const std::vector<std::complex<double>>::const_iterator &iterator) const {
  // Return the square root of the squared value.
  return std::sqrt(getAmplitudeSquared(iterator));
}

double Fourier::getAmplitudeSquared(const std::vector<std::complex<double>>::const_iterator &iterator) const {
  // Return the norm for the corrected value.
  return std::norm(getCorrected(iterator));

}

double Fourier::getPhase(const std::vector<std::complex<double>>::const_iterator &iterator) const {
  // Return the phase angle for the corrected value.
  return std::arg(getCorrected(iterator));
}

std::complex<double> Fourier::getCorrected(const std::vector<std::complex<double>>::const_iterator &iterator) const {
  const int index = getIndex(iterator);
  
  // Lookup the combine the frequency of the selected static part.
  const std::complex<double> static_part = static_estimate.get() * precomputed_frequency_vector_static[index];
  
  // Lookup the combine the frequency of the selected linear part.
  const std::complex<double> linear_part = slope_estimate.get() * precomputed_frequency_vector_linear[index];
  
  // Return the corrected value.
  return *iterator - static_part - linear_part;

}

double Fourier::getAngularVelocity(const std::vector<std::complex<double>>::const_iterator &iterator) const {
  return getAngularVelocityFromFrequency(getFrequency(iterator));
}

double Fourier::getAngularVelocityFromFrequency(double frequency) const {
  return 2 * std::numbers::pi * frequency;
}

double Fourier::getAngularVelocityFromIndex(int index) const {
  return getAngularVelocityFromFrequency(getFrequencyFromIndex(index));
}

double Fourier::getAngularIncrementFromVelocity(double velocity) const {
  return velocity / static_cast<double>(size);
}

double Fourier::getAngularIncrementFromFrequency(double frequency) const {
  return getAngularIncrementFromVelocity(getAngularVelocityFromFrequency(frequency));
}

double Fourier::getAngularIncrementFromIndex(int index) const {
  return getAngularIncrementFromFrequency(getFrequencyFromIndex(index));
}
