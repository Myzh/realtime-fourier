#pragma once

#include <numbers>
#include <type_traits>
#include <concepts>

template<std::floating_point T>
class LowPassFilter {
public:
  LowPassFilter(T frequency, T time_delta) : alpha((2 * std::numbers::pi * frequency * time_delta) / (2 * std::numbers::pi * frequency * time_delta + 1)), last_output(0) {}

  T push(T value) {
    last_output = alpha * value + (1 - alpha) * last_output;

    return get();
  }

  T get() const {
    return last_output;
  }

protected:
  const T alpha;

  T last_output;
};
